
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 1000
    });

    $('#contactar').on('show.bs.modal', function (e) {
        console.log('el modal se está mostrando');
        $('#contactarBtn').removeClass('btn-outline-success');
        $('#contactarBtn').addClass('btn-primary');
        $('#contactarBtn').prop('disabled', true);
    });

    $('#contactar').on('shown.bs.modal', function (e) {
        console.log('el modal se mostró');
    });

    $('#contactar').on('hide.bs.modal', function (e) {
        console.log('el modal se oculta');
    });

    $('#contactar').on('hidden.bs.modal', function (e) {
        console.log('el modal se ocultó');
        $('#contactarBtn').prop('disabled', false);
    });
});
